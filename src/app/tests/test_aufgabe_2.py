import unittest

from src.app.routes.admin import get_dynamic_table
from src.app.routes.user import profile_home_blueprint


class TestOrderTable(unittest.TestCase):

    # TODO Codequalität anpassen
    def test_new_feature(self):
        # test_input NICHT anpassen
        test_input = [
            [1, 12, "B: Test Product"],
            [2, 12, "B: Test Product"],
            [3, 12, "B: Test Product"],
            [4, 1, "A: Test Product"],
            [5, 1, "A: Test Product"],
            [6, 12, "Z: Test Product"]
        ]

        dynamic_table = get_dynamic_table(test_input)

        # Anpassung des expected_outcome, um den Test zu fixen
        expected_outcome = [
            ["Order ID", "A: Test Product", "B: Test Product", "Z: Test Product"],
            [1, 0, 1, 0],  # Order ID 1: 0 A, 1 B, 0 Z
            [2, 0, 1, 0],  # Order ID 2: 0 A, 1 B, 0 Z
            [3, 0, 1, 0],  # Order ID 3: 0 A, 1 B, 0 Z
            [4, 1, 0, 0],  # Order ID 4: 1 A, 0 B, 0 Z
            [5, 1, 0, 0],  # Order ID 5: 1 A, 0 B, 0 Z
            [6, 0, 0, 1],  # Order ID 6: 0 A, 0 B, 1 Z
            ["Total", 2, 3, 1]  # Total: 2 A, 3 B, 1 Z
        ]

        # Testet, ob expected_outcome gleich dem return Wert der Methode get_dynamic_table ist
        self.assertTrue(expected_outcome == dynamic_table)

if __name__ == '__main__':
    unittest.main()
